/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_group.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:30:44 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/16 16:30:46 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	print_group(const t_file *file, const size_t max)
{
	const char *group;

	group = get_group(file);
	align(ft_strlen(group), max);
	ft_putstr(group);
	ft_putstr("  ");
}
