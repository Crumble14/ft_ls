/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   param.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:35:11 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 20:11:43 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

t_bool	is_param(const char *str)
{
	if (ft_strlen(str) <= 1)
		return (FALSE);
	if (str[0] != '-')
		return (FALSE);
	if (ft_strequ(str, "--"))
		return (FALSE);
	return (TRUE);
}

void	param_to_flag(const char param, t_flags *flags)
{
	if (param == 'l')
		*(flags) |= LONG_F;
	else if (param == 'R')
		*(flags) |= RECURSE_F;
	else if (param == 'a')
		*(flags) |= HIDDEN_F;
	else if (param == 'r')
		*(flags) |= REVERSE_F;
	else if (param == 't')
		*(flags) |= TIME_F;
	else if (param == '1')
		*(flags) |= ONELINE_F;
	else
		invalid_param(param);
}
