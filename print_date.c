/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_date.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:30:34 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:33:04 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	print_date(const t_file *file)
{
	const char *date;

	date = get_date(file);
	ft_putstr(date);
	free((void*)date);
	ft_putchar(' ');
}
