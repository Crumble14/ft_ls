/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_perms.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:30:57 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/21 18:10:34 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static void				print_letter(char c, const t_bool empty)
{
	if (empty && (c >= 'a' && c <= 'z'))
		c += ('A' - 'a');
	ft_putchar(c);
}

void					print_perms(const t_file *file)
{
	mode_t mode;
	t_bool empty;

	mode = file->st.st_mode;
	empty = (!(mode & 0777));
	ft_putchar(file->type);
	print_letter(mode & S_IRUSR ? 'r' : '-', empty);
	print_letter(mode & S_IWUSR ? 'w' : '-', empty);
	if (mode & S_ISUID)
		print_letter('s', empty);
	else
		print_letter(mode & S_IXUSR ? 'x' : '-', empty);
	print_letter(mode & S_IRGRP ? 'r' : '-', empty);
	print_letter(mode & S_IWGRP ? 'w' : '-', empty);
	if (mode & S_ISGID)
		print_letter('s', empty);
	else
		print_letter(mode & S_IXUSR ? 'x' : '-', empty);
	print_letter(mode & S_IROTH ? 'r' : '-', empty);
	print_letter(mode & S_IWOTH ? 'w' : '-', empty);
	if (mode & S_ISVTX)
		print_letter('t', empty);
	else
		print_letter(mode & S_IXOTH ? 'x' : '-', empty);
	ft_putstr("  ");
}
