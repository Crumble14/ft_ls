/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 14:23:57 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 15:49:52 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		print_error(const int stop)
{
	ft_putstr_fd("ft_ls: ", 2);
	ft_putstr_fd(strerror(errno), 2);
	ft_putchar_fd('\n', 2);
	if (stop)
		exit(-1);
	else
		errno = 0;
}

void		print_file_error(const int stop, const char *name)
{
	t_bool no_name;

	no_name = FALSE;
	if (name)
	{
		if ((no_name = (*name == '\0')))
			name = "fts_open";
		ft_putstr_fd("ft_ls: ", 2);
		ft_putstr_fd(name, 2);
		ft_putstr_fd(": ", 2);
		ft_putstr_fd(strerror(errno), 2);
		ft_putchar_fd('\n', 2);
	}
	if (stop || no_name)
		exit(-1);
	else
		errno = 0;
}

static void	print_usage(void)
{
	ft_putstr_fd("usage: ft_ls [-lRart1] [file ...]\n", 2);
}

void		invalid_param(const char param)
{
	ft_putstr_fd("ft_ls: illegal option -- ", 2);
	ft_putchar_fd(param, 2);
	ft_putchar_fd('\n', 2);
	print_usage();
	exit(-1);
}
