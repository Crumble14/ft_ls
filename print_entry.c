/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_entry.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 18:25:23 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:28:00 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		print_path(const char *path, const t_bool line_feed,
	const t_bool current)
{
	if (!path)
		return ;
	if (!current && ft_strequ(path, "."))
		return ;
	if (line_feed)
		ft_putchar('\n');
	ft_putstr(path);
	ft_putstr(":\n");
}

void		print_blocks(const t_list *lst)
{
	const t_list	*l;
	size_t			total;

	l = lst;
	total = 0;
	while (l)
	{
		total += ((const t_file*)l->content)->st.st_blocks;
		l = l->next;
	}
	ft_putstr("total ");
	ft_putnbr(total);
	ft_putchar('\n');
}

void		print_entry(const t_file *file, const t_flags flags)
{
	t_list *lst;

	if (!file)
		return ;
	lst = ft_lstnew(file, sizeof(t_file));
	if (flags & LONG_F)
		print_files_list(lst, flags, TRUE);
	else
		simple_print(lst, flags);
	ft_lstdel(&lst, del_lst);
}

void		print_entries(const t_list *files, const t_flags flags)
{
	if (!files)
		return ;
	if (flags & LONG_F)
		print_files_list(files, flags, FALSE);
	else
		simple_print(files, flags);
}
