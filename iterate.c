/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iterate.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 17:47:14 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 20:11:26 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static void		handle_file(t_list **lst, const char *path,
	const struct dirent *entry)
{
	const char	*p;
	t_file		*file;

	if (!lst || !path || !entry)
		return ;
	p = ft_path_merge(path, entry->d_name);
	file = new_file(p);
	free((void*)p);
	if (!file)
	{
		print_file_error(FALSE, path);
		return ;
	}
	ft_lstpush(lst, ft_lstnew(file, sizeof(t_file)));
	free((void*)file);
}

t_list			*iterate_dir(const t_file *file, const t_flags flags)
{
	t_list				*lst;
	DIR					*dir;
	struct dirent		*entry;

	errno = 0;
	if (!file)
		return (NULL);
	lst = NULL;
	if (file->type != DIRECTORY)
		return (NULL);
	if (!(dir = opendir(file->path)))
		return (NULL);
	while ((entry = readdir(dir)))
	{
		if (!(flags & HIDDEN_F) && entry->d_name[0] == '.')
			continue ;
		handle_file(&lst, file->path, entry);
	}
	closedir(dir);
	return (lst);
}
