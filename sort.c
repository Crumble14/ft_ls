/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 15:55:07 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 20:17:20 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

inline static int	name_cmp(const char *v1, const char *v2, const t_bool param)
{
	if (!v1 || !v2)
		return (0);
	if (param && (ft_strchr(v1, '/') || ft_strchr(v2, '/'))
		&& (ft_strlen(v1) != ft_strlen(v2)))
		return (ft_strlen(v1) - ft_strlen(v2));
	return (ft_strcmp(v1, v2));
}

static int			time_name_cmp(const t_file *f1, const t_file *f2,
	const t_bool param)
{
	if (!f1 || !f2)
		return (0);
	if (f1->st.st_mtime == f2->st.st_mtime)
		return (name_cmp(f1->name, f2->name, param));
	return (f2->st.st_mtime - f1->st.st_mtime);
}

static int			compare(t_list *l1, t_list *l2,
	const t_flags flags, const t_bool file)
{
	const t_file	*f1;
	const t_file	*f2;
	int				r;

	if (!l1 || !l2)
		return (0);
	f1 = (file ? l1->content : new_file(l1->content));
	f2 = (file ? l2->content : new_file(l2->content));
	if (!f1 && !f2)
		r = name_cmp(l1->content, l2->content, !file);
	else if (!f1 || !f2)
		r = ((f1 ? 1 : 0) - (f2 ? 1 : 0));
	else if (flags & TIME_F)
		r = (flags & REVERSE_F ? -1 : 1) * time_name_cmp(f1, f2, !file);
	else
		r = (flags & REVERSE_F ? -1 : 1) * name_cmp(f1->path, f2->path, !file);
	if (!file)
	{
		free_file((void*)f1);
		free_file((void*)f2);
	}
	return (r);
}

void				sort(t_list *lst, const t_flags flags)
{
	t_list *l1;
	t_list *l2;

	if (!lst)
		return ;
	l1 = lst;
	while (l1)
	{
		l2 = l1->next;
		while (l2)
		{
			if (compare(l1, l2, flags, FALSE) > 0)
				list_swap(l1, l2);
			l2 = l2->next;
		}
		l1 = l1->next;
	}
}

void				sort_files(t_list *files, const t_flags flags)
{
	t_list *l1;
	t_list *l2;

	if (!files)
		return ;
	l1 = files;
	while (l1)
	{
		l2 = l1->next;
		while (l2)
		{
			if (compare(l1, l2, flags, TRUE) > 0)
				list_swap(l1, l2);
			l2 = l2->next;
		}
		l1 = l1->next;
	}
}
