/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_owner.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:30:54 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/16 16:30:56 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	print_owner(const t_file *file, const size_t max)
{
	const char *owner;

	owner = get_owner(file);
	align(ft_strlen(owner), max);
	ft_putstr(owner);
	ft_putstr("  ");
}
