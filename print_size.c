/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:31:00 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/17 14:09:54 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

size_t		sizelen(const size_t size)
{
	return (size > 9 ? sizelen(size / 10) + 1 : 1);
}

static void	putsize(const size_t size)
{
	if (size > 9)
		putsize(size / 10);
	ft_putchar('0' + (size % 10));
}

void		print_size(const t_file *file, const size_t max)
{
	size_t size;

	size = get_size(file);
	align(sizelen(size), max);
	putsize(size);
	ft_putchar(' ');
}
