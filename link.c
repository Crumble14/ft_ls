/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 19:01:09 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:23:25 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

const char	*resolve_path(const char *p1, const char *p2)
{
	char *s;
	char *p;
	char *s1;

	if (!p1 || !p2)
		return (NULL);
	if (*p1 == '\0' || *p2 == '\0')
		return (NULL);
	if (*p2 == '/')
		return (ft_strdup(p2));
	if (!(s = ft_strdup(p1)))
		return (NULL);
	if ((p = ft_strrchr(s, '/')))
		p[1] = '\0';
	s1 = ft_path_merge((p ? s : "."), p2);
	free((void*)s);
	return (s1);
}

t_file		*follow_link(const t_file *file)
{
	char		buffer[PATH_MAX];
	const char	*path;
	t_file		*f;

	if (!file)
		return (NULL);
	if (file->type != LINK)
		return (NULL);
	ft_bzero(buffer, sizeof(buffer));
	if (readlink(file->path, buffer, sizeof(buffer)) <= 0)
		return (NULL);
	if (!(path = resolve_path(file->path, buffer)))
		return (NULL);
	f = new_file(path);
	free((void*)path);
	return (f);
}

t_bool		points_to_dir(const t_file *file)
{
	const t_file	*f;
	t_bool			r;

	if (!file)
		return (FALSE);
	if (file->type == DIRECTORY)
		return (TRUE);
	if (file->type != LINK)
		return (FALSE);
	f = follow_link(file);
	errno = 0;
	r = (f && f->type == DIRECTORY);
	free_file(f);
	return (r);
}
