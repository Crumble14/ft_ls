/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_info.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 16:06:05 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:32:47 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

size_t		get_links(const t_file *file)
{
	return (file->st.st_nlink);
}

const char	*get_owner(const t_file *file)
{
	struct passwd	*pwd;

	if (!(pwd = getpwuid(file->st.st_uid)))
		print_error(1);
	return (pwd->pw_name);
}

const char	*get_group(const t_file *file)
{
	struct passwd	*pwd;
	struct group	*grp;

	if (!(pwd = getpwuid(file->st.st_uid)))
		print_error(1);
	if (!(grp = getgrgid(file->st.st_gid)))
		print_error(1);
	return (grp->gr_name);
}

size_t		get_size(const t_file *file)
{
	return (file->st.st_size);
}

const char	*get_date(const t_file *file)
{
	long	diff;
	char	*date;
	char	*s;

	diff = time(NULL) - file->st.st_mtime;
	if (diff < 0)
		diff = -diff;
	date = ctime(&file->st.st_mtime);
	date = ft_strtrim(date);
	ft_memmove(date, date + 4, ft_strlen(date) - 4 + 1);
	s = ft_strrchr(date, ':');
	ft_memmove(s, s + 3, 3);
	if (diff >= 16070400)
	{
		s = ft_strrchr(s, ' ');
		ft_memmove(date + 8, s + 1, ft_strlen(s + 1) + 1);
		date[7] = ' ';
	}
	else
		date[ft_strlen(date) - 8] = '\0';
	return (date);
}
